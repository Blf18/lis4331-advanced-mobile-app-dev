> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### Project 1 Requirements:


 

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's follow-up screen
* Screenshot of running application's play and pause user interfaces
* Screenshot of skill sets 7-9

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of MyMusic App*:

Splash Screen             |  Opening Screen
:-------------------------:|:-------------------------:
![Splash Screen Screenshot](img/P1SplashScreen.png)  |  ![Opening Screenshot](img/Opening.png)

Play Screen             |  Pause Screen
:-------------------------:|:-------------------------:
![Play Screen Screenshot](img/play.png)  |  ![Pause Screenshot](img/pause.png)

*Gif of MyMusic Running*

![MyMusic GIF](img/p1gif.gif)

*Screenshot of Skill Set 7: Measurement Conversion*

![Time Conversion Screenshot](img/Q7.png)

*Screenshot of Skill Set 8: Distance Calculator*

![Measurement Conversion Screenshot](img/Q8.png)

*Screenshot of Skill Set 9: Multiple Selection Lists*:

Unselected List             | Multiple Interval Selection  |
:-------------------------:|:-------------------------: 
![Unselected List Screenshot](img/Q91.png)  |  ![Multiple Interval Screenshot](img/Q92.png)

]



