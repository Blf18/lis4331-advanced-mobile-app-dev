> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### Assignment 2 Requirements:

*Four Parts:*

1. Drop-down menu for total number of guests: 1-10
2. Drop-down menu for tip percentage: 0-25
3. Must add background color or theme
4. Must create and display launcher icon image
 

#### README.md file should include the following items:

* Screenshot of running application unpopulated user interface
* Screenshot of running application populated user interface
* Screenshot of Skill sets 1-3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Tip Calculator*:

Unpopulated User Interface             |  Populated User Interface
:-------------------------:|:-------------------------:
![Unpopulated Screenshot](img/unpopulated.png)  |  ![Populated Screenshot](img/populated.png)

*Screenshot of Skill Sets 1-2*:

Non-OOP Circle             | Multiple Number  |
:-------------------------:|:-------------------------: 
![Circle Screenshot](gif/PaintCal.gif)  |  ![Multiple Number Screenshot](img/MultipleNumber.png)

*Screenshot of Skill Set 3*

![A1 ERD Screenshot](img/NestedStructure2.png)



