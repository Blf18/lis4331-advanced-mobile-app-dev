> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### Assignment 4 Requirements:


 

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's invalid screen
* Screenshot of skill sets 10-12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Home Mortgage App*:

Splash Screen             |  Main Screen
:-------------------------:|:-------------------------:
![Splash Screen Screenshot](img/splash.png)  |  ![Main Screenshot](img/main.png)

Incorrect Number Of Years Screen             |  Pause Screen
:-------------------------:|:-------------------------:
![Play Screen Screenshot](img/incorrect.png)  |  ![Pause Screenshot](img/valid.png)


*Screenshot of Skill Set 10: Travel Time*

![Travel Time Screenshot](img/ss10.png)

*Screenshot of Skill Set 11: Product Class*

![Measurement Conversion Screenshot](img/ss11.png)

*Screenshot of Skill Set 11: Book Inherits*

![Measurement Conversion Screenshot](img/ss12.png)






