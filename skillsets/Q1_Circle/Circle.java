import java.util.Scanner;

public class Circle
{
  public static void main (String [] args)
  {
    System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
    System.out.println("Must use Java's built-in PI constant, print(), and formatted to 2 decimal places.");
    System.out.println("Must *only* permit numeric entry.\n");
    
    Scanner input = new Scanner (System.in);
    double radius = 0.0;
    
    System.out.print("Enter circle radius: ");
    while (!input.hasNextDouble())
    {
      System.out.println("Not valid number!\n");
      input.next();
      System.out.print("Please try again. Enter circle radius: ");
    }
    radius = input.nextDouble();
    
    System.out.printf("\nCircle diameter: %.2f\nCircumference: " + "%.2f\nArea: %.2f\n", (2*radius), (2*Math.PI*radius), (Math.PI*radius*radius));
  }
}
  
