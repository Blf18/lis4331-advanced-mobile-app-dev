class Person
{
private String code; 
private String description; 
private int price;
private String author;
public Person()
{
System.out.println("\nInside product book default constructor."); 
code = "abc123"; 
description = "My Widget"; 
price = 49;
author = "John Doe";
}
    public Person(String code, String description, int price, String author)
{
System.out.println("\nInside product constructor with parameters."); 
this.code = code;
this.description = description; 
this.price = price;
this.author = author;
}
public String getCode()
{
return code;
}
public void setCode(String cd)
{
code = cd;
}
 public String getDescription()
{
return description;
}
public void setDescription(String ds)

{
description = ds;
}
public int getPrice()
{
return price;
}
    public void setPrice(int p)
    {
        price = p;
    }
    public String getAuthor()
    {
	return author;
    }
    public void setAuthor(String au)
    {
	code = au;
    }
public void print()
{
    System.out.println("\nCode:jkl187 " + code + ", Description: " + description +  ", Price: " + price + ", Author: Brandt Frazier" + author);
}
}
