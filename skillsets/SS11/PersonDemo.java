import java.util.Scanner;

class PersonDemo
{
    public static void main(String[] args)
    {
	String cd = "" ;
	String ds = "" ;
	int p = 0;
	String au = "" ;
	Scanner sc = new Scanner(System.in);
	
	System.out.println("n/////Below are default constructor values://///");
	Person v1 = new Person(); 
	System.out.println("\nCode= " +  v1.getCode());
	System.out.println("Description = " + v1.getDescription());
	System.out.println("Price = " + v1.getPrice());
	System.out.println("Author = " + v1.getAuthor());
			   
	System.out.println("\n/////Below are user-entered values://///");
			   

	System.out.print("\nCode: ");
	cd = sc.nextLine();
			   
        System.out.print("Description: ");
	ds = sc.nextLine();
			   
        System.out.print("Price: ");
	p = sc.nextInt();

	System.out.print("Author: ");
	au = sc.nextLine();

	Person v2 = new Person(cd, ds, p, au);
	System.out.println("\nCode = " + v2.getCode());
	System.out.println("Description = " + v2.getDescription());
	System.out.println("Price ="  + v2.getPrice());
	System.out.println("Author = " + v2.getAuthor());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
	 v2.setCode("jkl187");
	 v2.setDescription("Another Widget");
	 v2.setPrice(99);
	 v2.setAuthor("Brandt Frazier");
	 v2.print();
	 
			   }
			   }
