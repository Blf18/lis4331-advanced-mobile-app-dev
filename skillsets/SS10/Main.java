import java.util.Scanner;

class Main
{
    public static void main(String args[])
    {
        double miles = 0.0;
        double mph = 0.0;
        double validMiles = 0.0;
        double validMPH = 0.0;
        
        String choice = "";
        
        Scanner sc = new Scanner(System.in);
        
        Methods.getRequirements();
        
        do
        {
            miles = Methods.validateMilesDataType();
            validMiles = Methods.validateMilesRange(miles);
            
            mph = Methods.validateMPHDataType();
            validMPH = Methods.validateMPHRange(miles);
            
            Methods.calculateTravelTime(validMiles, validMPH);
            
            System.out.print("\nContinue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
        while (choice.equalsIgnoreCase("y"));
    }
    
}
