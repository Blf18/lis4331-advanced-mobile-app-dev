import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brandt Frazier-Allen");
        System.out.println("Program calculates approximate travel time.");
        System.out.println();
    }
    
    public static double validateMilesDataType()
    {
        double miles = 0.0;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter miles: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Invalid input--miles must be a number: ");
            sc.next();
        }
        miles = sc.nextDouble();
        return miles;
    }
    
    public static double validateMilesRange(double miles)
    {
        while (miles <= 0 || miles > 3000)
        {
            System.out.println("Miles must be greater than 0, and no more than 3000.\n");
            miles = validateMilesDataType();
        }
        return miles;
    }
    
    public static double validateMPHDataType()
    {
        double mph = 0.0;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter mph: ");
        while(!sc.hasNextDouble())
        {
             System.out.print("Invalid input--mph must be a number: ");
            sc.next();
        }
        mph = sc.nextDouble();
        return mph;
    }
    
    public static double validateMPHRange(double mph)
    {
        while (mph <= 0 || mph > 100)
        {
            System.out.println("MPH must be greater than 0, and no more than 100.\n");
            mph = validateMPHDataType();
        }
        return mph;
    }
    
    public static void calculateTravelTime(double miles, double mph)
    {
        double hours = 0.0;
        int minutes = 0;
        int hoursInt = 0;
        int minutesRemainder = 0;
        
        hours = miles/ mph;
        
        minutes = (int) (hours * 60);
        
        hoursInt = minutes / 60;
        minutesRemainder = minutes % 60;
        
        System.out.println("Estimated travel time: " + hoursInt + " hr(s)" + minutesRemainder + "Minutes");
        
    }
    

}

