> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### Assignment 5 Requirements:


 

#### README.md file should include the following items:

* Screenshot of running application’s main screen (list of articles – activity_items.xml)* Screenshot of running application’s individual article (activity_item.xml)
* Screenshots of running application’s default browser (article link);
* Screenshot of skill sets 13-15

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of News Feed App*:

Items Activity            | Item Activity 
:-------------------------:|:-------------------------:
![Items Activity  Screenshot](img/itemsactivity.png)  |  ![Item Activity  Screenshot](img/itemsactivity.png)


*Screenshot of Read More...*:
![Travel Time Screenshot](img/readmore.png)


*Screenshot of Skill Set 13: Write/Read File*

![Travel Time Screenshot](img/ss13.png)

*Screenshot of Skill Set 14: Simple Interest Calculator*

![Measurement Conversion Screenshot](img/ss14.png)

*Screenshot of Skill Set 15: Array Demo*

![Measurement Conversion Screenshot](img/ss15.png)






