> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### Assignment 3 Requirements:


 

#### README.md file should include the following items:

* Screenshot of running application splash screen
* Screenshot of running application populated user interface
* Screenshot of running application toast notification
* Screenshot of running application converted currency user interface
* Screenshot of Skill sets 4-6

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Currency Converter*:

Splash Screen             |  Unpopulated UI
:-------------------------:|:-------------------------:
![Splash Screen Screenshot](img/splash.png)  |  ![UnPopulated Screenshot](img/UI.png)

Toast Notification             |  Converted Currency
:-------------------------:|:-------------------------:
![Splash Screen Screenshot](img/toast.png)  |  ![UnPopulated Screenshot](img/Converted.png)

*Screenshot of Skill Set 4: Time Conversion*

![Time Conversion Screenshot](img/TimeConversion.png)

*Screenshot of Skill Set 5: Even or Odd*:

Even Number             | Odd Number  |
:-------------------------:|:-------------------------: 
![Even Screenshot](img/Even.gif)  |  ![Odd Number Screenshot](img/odd.gif)

*Screenshot of Skill Set 6: Paint Cost*

![Paint Cost Screenshot](img/PaintCal.gif)



