> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Screenshot of running application unpopulated user interface
	- Screenshot of running application populated user interface
	- Screenshot of Skill sets 1-3
2. [A3 README.md](a3/README.md "My A3 README.md file")
	* Screenshot of running application splash screen
	* Screenshot of running application populated user interface
	* Screenshot of running application toast notification
	* Screenshot of running application converted currency user interface
	* Screenshot of Skill sets 4-6

2. [A4 README.md](a4/README.md "My A4 README.md file")
	* Screenshot of running application's splash screen
	* Screenshot of running application's invalid screen
	* Screenshot of skill sets 10-12

2. [A5 README.md](a5/README.md "My A5 README.md file")
	* Screenshot of running application’s main screen (list of articles – activity_items.xml)	* Screenshot of running application’s individual article (activity_item.xml)
	* Screenshots of running application’s default browser (article link);
	* Screenshot of skill sets 13-15

*Projects:*

2. [P1 README.md](p1/README.md "My P1 README.md file")
	* Screenshot of running application's splash screen
	* Screenshot of running application's follow-up screen
	* Screenshot of running application's play and pause user interfaces
	* Screenshot of skill sets 7-9

2. [P2 README.md](p2/README.md "My P2 README.md file")
	* Screenshot of running application's splash screen
	* Must use persistent data: SQLite database
	* Insert at least five users.
	* Must add background color(s) or theme.
	* Create and display launcher icon image.




