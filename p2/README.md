> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Brandt Frazier-Allen

### Project 2 Requirements:


 

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Must use persistent data: SQLite database
* Insert at least five users.
* Must add background color(s) or theme.
* Create and display launcher icon image.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project 2 Screenshots:

*Screenshot of My Users App*:

Splash Screen             |  Add User Screen
:-------------------------:|:-------------------------:
![Splash Screen Screenshot](img/splashp2.png)  |  ![Added User Screenshot](img/added.png)

Update User Screen             |  View Users Screen
:-------------------------:|:-------------------------:
![Updated User Screenshot](img/update.png)  |  ![View Users Screenshot](img/view.png)

*Deleting Users Screenshot*

![MyMusic GIF](img/delete.png)



